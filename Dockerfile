
## start with the Docker 'base R' Debian-based image
FROM rocker/r-devel:latest

RUN R --version

RUN apt-get install -y gnupg2 gnupg gnupg1

RUN apt-get install -y software-properties-common

RUN apt-get install -y r-base r-base-core r-recommended r-base-dev

RUN add-apt-repository -r 'ppa:c2d4u.team/c2d4u4.0+' \
    && apt-get update

RUN apt-get update -qq \
    && apt-get dist-upgrade -y \
    && apt-get install -t unstable -y --no-install-recommends \
        libquantlib0* \
        libxml2-dev \
        libssl-dev \
        qpdf \
        pandoc \
        pandoc-citeproc \
        libgmp3-dev \
        libmpfr-dev \
        libmpfr-doc

RUN RD -e 'install.packages(c("pkgload","testthat","devtools","roxygen2","callr","rvest","xml2","tidyverse","gmp","Rmpfr","knitr","rmarkdown","ggplot2","qpdf"),quiet=TRUE)'
